//SECTION Comparisons Query Operators


//$gt/$gte operator
/*
	-allows us to find the documents that have field number valued greater than or greater than or equal to a specified number
	SYNTAX:
	db.collectionName.find({field: {$gt: value}});
	db.collectionName.find({field: {$gte: value}});
*/
db.users.find({age: {$gt:50}});
db.users.find({age: {$gte:50}});

//$lt/$lte operator
/*
	-allows us to find documents that have field number values less than or less than or equal to specified number
	SYNTAX:
	db.collectionName.find({field: {$lt: value}});
	db.collectionName.find({field: {$lte: value}});
*/

db.users.find({age: {$lt:50}});
db.users.find({age: {$lte:50}});


//$ne operator
/*
	-allows us find documents that have field number values that are not equal to a specified number
		SYNTAX:
		db.collectionName.find({field: {$ne: value}});
*/
db.users.find({age: {$ne:82}});

//$in operator
/*
	-allows us to fiend documents with speicifc match criteria

	within value1 to value2
	SYNTAX:
	db.collectionName.find({field: {$in: value}});
*/
db.users.find({lastName: {$in:["Hawking","Smith"]}});

//when looking for a nested array using $in operator
db.users.find({courses: {$in:["HTML", "React"]}});








//SECTION - Logical Query Operators


//$or - operator
/*
	-allows us to find documents matching a single criteria from multiple provided search criteria
	SYNTAX:
		db.collectionName.find({$or: [{fieldA: valueA},{fieldB:valueB}]});
*/
db.users.find({$or: [{firstName: "Neil"},{age: 21}]});
//try to fuse $or operator and gt operator (used in age)
db.users.find({$or: [{firstName: "Neil"},{age: {$gt:21}}]});

//$and - operator
/*
	-allows us to fiend matching multiple criteria in a single field
	SYNTAX:
	db.collectionName.find({$and: [{fieldA: valueA},{fieldB:valueB}]});
*/
	db.users.find(
		{
			$and: [
			{age:{$ne: 82}},
			{age: {$ne: 76}
		}
		]
	});


//SECTION - Field Projection
/*
	-retrieving documents are common operations that we do and by default MongoDB queries return the whole document as response
	-when dealing with complex data structure, there might be instances when fields are not useful for the query that we are trying to accomplish

*/
//INCLUSION
/*
	-allows us tp include specific fields only when retrieving documents
	1/true means true,
	0/false means false, 
	SYNTAX:
	db.collectionName.find({criteria},{field:1});
*/
db.users.find(
	{firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

//EXCLUSION
/*
	-allows us to exclude specific fields only when retrieving documents
	-the value provided is 0 to denote that the field is being excluded
	SYNTAX:
	db.collectionName.find({criteria},{field:0});
	1/true means true,
	0/false means false, 
*/

/*SUPPRESSING OF ID FIELD
		
*/

db.users.find(
	{firstName: "Neil" },
	{
         _id : 0,
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

//another way of coding
db.users.find(
	{firstName: "Jane" },
	{
		firstName: 1,
		lastName: 1,
		contact: {
                    phone: 1
                    }
	}
);

//Suppressing specific fields in embedded documents

db.users.find(
	{ firstName: "Jane" },
	{
		"contact.phone": 0
	}
)

//Project Speicifc Array Elements in the returned Array

//$slice operator -allows us to retrieve only 1 element that matches the search criteria

db.users.find(
	{
		"nameArr":{nameA: "Juan"}
	},
	{
		nameArr:
			{$slice : 1}
	}
);


//SECTION - Evaluation Query Operators
//$regex operator
/*
	-allows us to find documents that match a specific string pattern using regular expressions
	SYNTAX:
	db.collectionName.find({field: $regex: "pattern", $option: "$optionsValue"});
*/

db.users.find(
{
	firstName: 
	{
		$regex: "n"
	}

})

//with $options
db.users.find(
{
	firstName: 
	{
		$or: {
		$regex: "n",
		$regex: "N",
        }        
	}

})